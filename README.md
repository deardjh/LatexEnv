# LatexEnv
Prepare latex environment for paper writing.

Tex是Donald Ervin Knuth编写的功能强大得排版软件, 是命令行格式的,用来排版高质量的书籍,特别是包含有数学公式的书籍。

Latex 是对 TEX 的封装和拓展,实际上就是用 TEX 语言编写的一组宏代码,拥有比原来拥有比原来Tex格式更为规范的命令和一整套预定义的格式，隐藏了不少排版方面的细节，可以让完全不懂排版理论的学者们也可以比较容易地将书籍和文稿排版出来。

### 本机配置

![](img/1.png)

### 软件需要

- TexLive (texlivexxx.iso)
- TeXstudio
- Miktex

##### TexLive

TexLive 是 Tex 的一种比较流行的**发行版**，它是由 TUG（TEX User Group，TEX 用户组）发布的，可以在类 UNIX/Linux、Mac OS X 和 Windows 等不同的操作系统平台下安装使用，并且提供相当可靠的工作环境。

- 安装

  [Ubuntu下 TeX Live 2018 的安装与配置](https://blog.csdn.net/engreal/article/details/80704755)

  [Deepin Linux 安装和搭建LaTex环境](https://zhuanlan.zhihu.com/p/40053417)

##### Texstudio

TeXstudio是一个**编写LaTeX文档的集成开发环境**，它具有拼写和语法检查、代码折叠、扩展文本导航、代码自动完成以及语法高亮等功能，同时内置 PDF 阅读器、集成的浏览器等多种辅助工具。

- 安装

```shell
sudo apt-get install texstudio
```

- 卸载

```shell
sudo apt-get remove texstudio
```

##### Miketex

MiKTeX（发音为*mick-tech*）是**TeX / LaTeX及相关程序的最新实现**，是LaTex的一个发布版本，Windows在<http://www.miktex.org/>下载安装即可。MiKTeX附带了TeXworks，一个TeX前端，您可以使用它来编辑和预览LaTeX文档。

- 安装

  [miktex](https://miktex.org/)

  [install-miktex-unx](https://miktex.org/howto/install-miktex-unx)

- 其他包

  安装完成有运行"Miktex console"进行配置,点击"Packages"标签，然后依次安装ctex和CJK包等(或者待编译时再按需安装也可),以便支持中文。

### 参考链接

- https://wiki.deepin.org/wiki/TeXstudio
- https://miktex.org/howto/install-miktex-unx
- https://zhuanlan.zhihu.com/p/42844087
- https://blog.csdn.net/qq_33429968/article/details/64919687?utm_source=blogxgwz4

#### CTAN

- [mirrors.tuna.tsinghua.edu.cn/CTAN](https://mirrors.tuna.tsinghua.edu.cn/CTAN/)
- [mirrors.aliyun.com/CTAN](http://mirrors.aliyun.com/CTAN/)
